# Creating a New User in Snowflake

- [ ] Create user using SECURITYADMIN role
- [ ] Create user specific role
- [ ] Assign user specific role
- [ ] Assign user in Okta
- [ ] Verify grants
- [ ] Update `roles.yml` and add a comment with a access request URL
