# ======
# Project Details
# ======
name: 'gitlab_snowflake'
version: '1.0'
profile: 'gitlab-snowflake'
require-dbt-version: ">=0.15.0"


# ======
# File Path Configs
# ======
source-paths: ["models"]
test-paths: ["tests"]
data-paths: ["data"]
macro-paths: ["macros"]
target-path: "target"
clean-targets:
    - "target"
    - "dbt_modules"

# ======
# Snowflake Configs
# ======
quoting:
    database: true
    identifier: false
    schema: false
query-comment: "{{ query_comment(node) }}"

# ======
# Run Hooks
# ======
on-run-start:
    - "{{ dbt_logging_start('on run start hooks') }}"
    - "{{resume_warehouse(var('resume_warehouse', false), var('warehouse_name'))}}"
    - "{{create_udfs()}}"

on-run-end:
    - "{{ dbt_logging_start('on run end hooks') }}"
    - "{{grant_usage_to_schemas(schema_name)}}"
    - "drop schema if exists {{ generate_schema_name('temporary') }}"
    - "{{suspend_warehouse(var('suspend_warehouse', false), var('warehouse_name'))}}"

# ======
# Seed Configs
# ======
seeds:
  enabled: true
  schema: staging
  quote_columns: False

  gitlab_snowflake:
    zuora_country_geographic_region:
      schema: analytics

# ======
# Model Configs
# ======
models:
  pre-hook:
    - "{{ logging.log_model_start_event() }}"
  post-hook:
    - "{{ logging.log_model_end_event() }}"
  vars:
    database: "raw"
    warehouse_name: "{{ env_var('SNOWFLAKE_TRANSFORM_WAREHOUSE') }}"

  # Logging Package
  logging:
    schema: meta
    post-hook:
      - "grant select on {{this}} to role reporter"

  # Snowplow Package
  snowplow:
    schema: "snowplow_{{ var('year', run_started_at.strftime('%Y')) }}_{{ var('month', run_started_at.strftime('%m')) }}"
    post-hook:
      - "grant usage on schema {{this.schema}} to role snowplow"
      - "grant select on {{this}} to role snowplow"
    tags: ["product"]
    vars:
      'snowplow:use_fivetran_interface': false
      'snowplow:events': "{{ref('snowplow_unnested_events')}}"
      'snowplow:context:web_page': "{{ref('snowplow_web_page')}}"
      'snowplow:context:performance_timing': false
      'snowplow:context:useragent': false
      'snowplow:timezone': 'America/New_York'
      'snowplow:page_ping_frequency': 30
      'snowplow:app_ids': ['gitlab', 'about', 'gitlab_customers']
      'snowplow:pass_through_columns': ['cf_formid','cf_elementid','cf_nodename','cf_type','cf_elementclasses','cf_value','sf_formid','sf_formclasses','sf_elements','ff_formid','ff_elementid','ff_nodename','ff_elementtype','ff_elementclasses','ff_value','lc_elementid','lc_elementclasses','lc_elementtarget','lc_targeturl','lc_elementcontent','tt_category','tt_variable','tt_timing','tt_label']

  # Snowflake Spend Package
  snowflake_spend:
    materialized: table
    xf:
      schema: analytics

  # GitLab Models
  gitlab_snowflake:
    materialized: view

    sources:
      schema: source

    staging:
      schema: staging
      zuora:
        xf:
          schema: analytics
          materialized: table

    bamboohr:
      schema: sensitive
      base:
        materialized: table

    customers:
      materialized: table
      base:
          schema: staging

    date:
      materialized: table

    engineering:
      materialized: table

    gitlab_data_yaml:
      materialized: table

    gitlab_dotcom:
      tags: ["product"]
      materialized: table
      base:
          schema: staging

    gitter:
      enabled: false
      transformed:
        materialized: table

    greenhouse:
      base:
        schema: sensitive
      rpt:
        schema: analytics
    
    handbook:
        materialized: table

    marts:
      revenue:
        base:
          materialized: table

    netsuite:
      materialized: table
      base:
        schema: staging

    pipe2spend:
      enabled: false
      materialized: table

    retention:
      materialized: table

    sfdc:
      base:
        schema: staging
      sensitive:
        schema: sensitive
      xf:
        materialized: table

    smau_events:
      tags: ["product"]
      materialized: table

    snowplow:
      tags: ["product"]
      xf:
        materialized: table

    snowplow_combined:
      tags: ["product"]

    version:
      tags: ["product"]
      base:
        schema: staging
      xf:
        materialized: table

    zendesk:
      xf:
        materialized: table

# ======
# Snapshot Configs
# ======
snapshots:
  gitlab_snowflake:
    target_database: "{{ env_var('SNOWFLAKE_LOAD_DATABASE') }}"
    target_schema: "snapshots"
    transient: false

    customers:
      tags: ["daily"]

    gitlab_dotcom:
      tags: ["daily"]

    license:
      tags: ["daily"]

    sfdc:
      tags: ["daily"]

    sheetload:
      tags: ["daily"]

    zuora:
      tags: ["weekly"]
